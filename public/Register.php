<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Registro</title>
        <!--Normalizar vista en todos los navegadores-->
        <link rel="stylesheet" href="CSS/normalize.css">
        <!--Icono Pestaña-->
        <link rel="shortcut icon" href="Imagenes/LogoSinFondoCuadradoIcono.ico">
        <!--Fuente GoogleFonts -- Caveat-->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@600&display=swap" rel="stylesheet">
        <!--Custom CSS-->
        <link rel="stylesheet" href="CSS/stylesL.css">
    </head>
    <body>
        <?php 
        $nombre = "";
        $apellido = "";
        $email = "";
        $usuario =""; 
        $contraseña = "";
        $contraseñaCheck = "";
            require 'includes/config/CDB.php';
            $db = conectarDB();
            if($_SERVER['REQUEST_METHOD']==='POST'){
                $nombre = mysqli_real_escape_string($db,$_POST['nombre']);
                $apellido =  mysqli_real_escape_string($db,$_POST['apellido']);
                $email =  mysqli_real_escape_string( $db, filter_var($_POST['correo'], FILTER_VALIDATE_EMAIL));
                $usuario =  mysqli_real_escape_string($db,$_POST['usuario']);
                $contraseña =  mysqli_real_escape_string($db,$_POST['contraseña']);
                $contraseñaCheck = $_POST['contraseñaCheck'];
                if($_POST['termCond']==='true'){
                    $termCond=1;
                }
                if($contraseña===$contraseñaCheck){
                    $query= "SELECT * FROM registro WHERE usuario = '${usuario}'";
                    $resultado= mysqli_query($db, $query);
                    if($resultado->num_rows){
                        $usuario="";
                        echo("<script>alert('El usuario ya existe') </script>");
                    }else{
                        //Inserar en base de datos
                        $contraseñaHash = password_hash($contraseña, PASSWORD_BCRYPT);
                        $query="INSERT INTO registro (nombre, apellido, email, usuario, pass, termCond, fechaReg, tipoUsuario) 
                        VALUES ('$nombre', '$apellido', '$email', '$usuario', '$contraseñaHash', $termCond, now(), DEFAULT)";
                        //echo($query);
                        $resultado = mysqli_query($db, $query);
                        if($resultado){
                            //echo"insertado Correctamente";
                            header('location: Login.php'); //Redireccionando
                        }
                    }
                   
                }
                else{
                    echo("<script>alert('No coinciden las contraseña, intente de nuevo.') </script>");
                }
            }
        ?>
        <div class="logo">
            <a href="index.php"><img src="Imagenes/PrimateCuadrado.webp" alt="PrimateLogo" width="150" height="150" class="nav-brand"></a>
        </div>    
        <section class="formularioRegistro">
            <h4>Registro</h4>
            <form class="formulario" method="POST" action="#">
                <input class= "controls" type="text" name="nombre" id="nombre" placeholder="Nombre" required="true" value="<?php echo("$nombre")?>">
                <input class= "controls" type="text" name="apellido" id="apellido" placeholder="Apellidos" required="true" value="<?php echo("$apellido")?>">
                <input class= "controls" type="email" name="correo" id="correo" placeholder="e-Mail" required="true" value="<?php echo("$email")?>">
                <input class= "controls" type="text" name="usuario" id="usuario" placeholder="Usuario" required="true" value="<?php echo("$usuario")?>"> 
                <input class= "controls" type="password" name="contraseña" id="contraseña" placeholder="Contraseña" required="true" minlength="8" pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$" title='La clave debe contener: Almenos 8 caracteres, Almenos una letra en mayúscula, Almenos una letra minuscula, Almenos un número.'>
                <input class= "controls" type="password" name="contraseñaCheck" id="contraseñaCheck" placeholder="Repita la contraseña" required="true">
                <p><input class= "checkB" type="checkbox" name="termCond" id="termCond" value="true" required="true">Estoy de acuerdo con <a class= "anima" href="#">Terminos y Condiciones</a></p>
                <input class="boton" type="submit" value="Registrarme">
            </form>
            <p><a class="anima" href="Login.php">Ya tengo una cuenta.</a></p>
        </section>
        <!--<script src="js/script.js"></script>-->
        <footer class="registro">
            <a href='https://www.freepik.es/vectores/fondo'target="_blank">Vector de Fondo creado por freepik - www.freepik.es</a>
        </footer>
    </body>
</html>

