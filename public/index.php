<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Primate Chocolatería</title>
        <!--Normalizar vista en todos los navegadores-->
        <link rel="stylesheet" href="CSS/normalize.css">
        <!--Icono Pestaña-->
        <link rel="shortcut icon" href="Imagenes/LogoSinFondoCuadradoIcono.ico">
        <!--Fuente GoogleFonts -- Caveat-->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@600&display=swap" rel="stylesheet">
        <!--Custom CSS-->
        <link rel="stylesheet" href="CSS/styles.css">
    </head>

    <body>
        <main>
          <div class="logo">
            <img src="Imagenes/PrimateCuadrado.webp" alt="PrimateLogo" width="400" height="400" class="nav-brand">
          </div>
          <nav>
            <a href="Login.php">Ingresa</a>
            <a href="catalogo.php">Catálogo de Productos</a>
            <a href="#">Pedidos Especiales</a>
          </nav>
        </main> 
        <footer>
          <a href='https://www.freepik.es/vectores/fondo'target="_blank">Vector de Fondo creado por freepik - www.freepik.es</a>
        </footer>
    </body>
</html>