<?php

function obtenerRegistro() : array /*Tipo de dato que retorna*/ {
    try {
        //Importar la conexión
        require 'config/CDB.php';
        //SQL
        //$sql = "SELECT * FROM registro"; //Pero no megusta así
        $consulta = mysqli_query($db,"SELECT * FROM registro"/*$sql*/);
        //Resultados
        $datos = [];
        $i=0;
        while($filas=mysqli_fetch_assoc($consulta)){
            $datos[$i]['id']=$filas['id'];
            $datos[$i]['usuario']=$filas['usuario'];
            $datos[$i]['tipoUsuario']=$filas['tipoUsuario'];
            $datos[$i]['fechaReg']=$filas['fechaReg'];
            $i++;
        }
        // echo "<pre>";
        // var_dump(/*json_encode(*/$datos/*)*/);
        // echo "</pre>";
        return $datos;

    } catch (\Throwable $th) {
        var_dump($th);
    }
}

obtenerRegistro();