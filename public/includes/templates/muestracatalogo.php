<?php 
    require 'includes/config/CDB.php';
    $db = conectarDB();
    //consultar
    $query = "SELECT * FROM catalogo ";
    $resultado = mysqli_query($db, $query);
?>
<main>
    <?php while($producto=mysqli_fetch_assoc($resultado)):?>
        <div class="contenidoProductos">
            <div class="anuncio">
                <h2> <?php echo($producto['nombreProducto'])?></h2>
            </div>
            <div>
                <img class="imagenCat" src="/Imagenes/imagenesProductos/<?php echo($producto['imagen']);?>" alt="Imagen producto">
            </div>
            <div>
                <p class="precio">Precio: $<?php echo(number_format(intval($producto['precio']),2,",","."));?></p>
            </div>
            <div>
                <p class="descrip"><?php echo($producto['descripcion'])?></p>
            </div>
            <div>
                <a class="botonDetalle" href="/admin/productos/actualizar/modificar.php<?php echo("?id=".$producto['id']);?>">Detalle de producto</a>
                <a class="botonCarrito" href="/admin/productos/actualizar/modificar.php<?php echo("?id=".$producto['id']);?>">Agregar al carrito</a>
            </div>
        </div>
    <?php endwhile; ?>
</main>