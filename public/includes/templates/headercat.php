<?php  
    if(!isset($_SESSION)){
        session_start();
    }
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Catálogo</title>
        <!--Normalizar vista en todos los navegadores-->
        <link rel="stylesheet" href="CSS/normalize.css">
        <!--Icono Pestaña-->
        <link rel="shortcut icon" href="Imagenes/LogoSinFondoCuadradoIcono.ico">
        <!--Fuente GoogleFonts -- Caveat-->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@600&display=swap" rel="stylesheet">
        <!--Custom CSS-->
        <link rel="stylesheet" href="/CSS/stylesCat.css">
    </head>
    <body>
        <header class= "header">
            <nav class="navega">
                <div class="logo">
                    <a href="/index.php"><img src="/Imagenes/PrimateCuadrado.webp" alt="PrimateLogo" width="150" height="150" class="nav-brand"></a>
                </div>
                <div class="opciones">
                    <a class="anima" href="catalogo.php">Productos</a>
                    <a class="anima" href="#">Pedidos especiales</a>
                    <?php
                        if($_SESSION){
                            if($_SESSION['auth']){
                                echo('<span class="usuario">');
                                echo($_SESSION['usuario']);
                                echo(' |'); 
                                echo('<input type="hidden" name="id" value="")>');
                                echo('<a class="anima" href="cerrarsesion.php"> Cerrar sesión</a></span>');
                            }
                        }else{
                            echo('<span><a class="anima" href="Login.php">Ingresa</a> | <a class="anima" href="Register.php">Registrate</a></span>');
                        }
                    ?>
                </div>
            </nav>
        </header>