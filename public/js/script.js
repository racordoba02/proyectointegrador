console.log('Funciona correctamente');

//Boton de registro-----------------------------
//Click unicamente usar para eventos generales, Un Like, agregar al carrito, entre otros
//En caso de un formualrio, lo más correcto es usar un Submit
//const registrar=document.querySelector('.boton')
//registrar.addEventListener('click', function(evento){ // Se puede pasar "Evento" y eso otorga dentro de la función el evento ocurrido y obtener toda ingo
//    //console.log(evento);
//    evento.preventDefault(); //Detiene la acción por defecto. Es util para validad formularios.
//    console.log('enviando formulario...');
//});

//Validar formulario
    //Objeto que se llenara con lso datos del formulario, Mismos nombres del ID del formulario = a los del objeto para que funcione
const datos={
    nombre:'',
    apellido:'',
    correo:'',
    usuario:'',
    contraseña:'',
    contraseñaCheck:'',
    termCond:''

}
    //Toma de los datos
const nombreInp = document.querySelector('#nombre');
const apellidoInp = document.querySelector('#apellido');
const emailInp = document.querySelector('#correo');
const usuarioInp = document.querySelector('#usuario');
const contraseñaInp = document.querySelector('#contraseña');
const contraseñaCheckInp = document.querySelector('#contraseñaCheck');
const termCondInp = document.querySelector('#termCond');
const formularioInp = document.querySelector('.formulario');

//Requerido
nombreInp.required=true;
apellidoInp.required=true;
emailInp.required=true;
usuarioInp.required=true;
contraseñaInp.required=true;
contraseñaInp.title='La clave debe contener:\n- Almenos 8 caracteres.\n- Almenos una letra en mayúscula.\n- Almenos una letra minuscula.\n- Almenos un número.';
contraseñaCheckInp.required=true;
termCondInp.required=true;

//Captar los datos del formulario
nombreInp.addEventListener('input', tomandoValores);
apellidoInp.addEventListener('input', tomandoValores);
emailInp.addEventListener('input', tomandoValores);
usuarioInp.addEventListener('input', tomandoValores);
contraseñaInp.addEventListener('input', tomandoValores);
contraseñaCheckInp.addEventListener('input', tomandoValores);
termCondInp.addEventListener('change', checkBox);

//Enviar formulario
formularioInp.addEventListener('submit', enviarFormulario);



//Funciones---------------------------------------------
function tomandoValores(evento){
    datos[evento.target.id]=evento.target.value;
    //console.log(evento);
    //console.log(datos);
    //console.log(evento.target.value); 
}
function checkBox(evento){
    if (termCondInp.checked) {
        datos[evento.target.id]= true
    } else {
        datos[evento.target.id]= false
    }
    //console.log(datos);
}
function enviarFormulario(evento){
    evento.preventDefault();
    //Validar formulario
    const { nombre, apellido, correo, usuario, contraseña, contraseñaCheck, termCond} = datos;
    if (contraseña != contraseñaCheck){
        window.alert('Las contraseñas no coinciden');
        //mostrarError('Las contraseñas no coninciden');
        return;
    }
    //Enviar formulario
    console.log('Enviando formulario...');
}
//function mostrarError(mensaje){
//      Forma distinta para realizar el mensaje de error y agregar HTML
//      con la creación del objeto y la clase, se pude maquillar con CSS
//     const error = document.createElement('P');//En mayusculas lo exige JS
//     error.textContent=mensaje;
//     error.classList.add('error');
//     formularioInp.appendChild(error);
//     //console.log(error)
//Quitar el mensaje de error de Function de arriba
//    setTimeout(()=>{
//        console.error.remove();
//    },5000);
// }

