<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Ingreso</title>
        <!--Normalizar vista en todos los navegadores-->
        <link rel="stylesheet" href="CSS/normalize.css">
        <!--Icono Pestaña-->
        <link rel="shortcut icon" href="Imagenes/LogoSinFondoCuadradoIcono.ico">
        <!--Fuente GoogleFonts -- Caveat-->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@600&display=swap" rel="stylesheet">
        <!--Custom CSS-->
        <link rel="stylesheet" href="CSS/stylesL.css">
    </head>
    <body>
        <?php 
            //Importe de base de datos
            require 'includes/config/CDB.php';
            $db = conectarDB();
            $usuario ="";
            //Iniciar sesion
            if($_SERVER['REQUEST_METHOD']==='POST'){
                $usuario = mysqli_real_escape_string($db, $_POST['usuario']);
                $contraseña =  mysqli_real_escape_string($db,$_POST['contraseña']);

                $query= "SELECT * FROM registro WHERE usuario = '${usuario}'";
                $resultado= mysqli_query($db, $query);
                if($resultado->num_rows){
                   $usuarioAux=mysqli_fetch_assoc($resultado);
                   //Verificar si las claves coninciden
                   $auxContraseña = password_verify($contraseña, $usuarioAux['pass'] );
                   if($auxContraseña){
                    session_start();
                    //Llenar el arreglos de la sesion
                    $_SESSION['nombre']= $usuarioAux['nombre'];
                    $_SESSION['apellido']= $usuarioAux['apellido'];
                    $_SESSION['email']= $usuarioAux['email'];
                    $_SESSION['usuario']= $usuarioAux['usuario'];
                    $_SESSION['numeroCont']= $usuarioAux['numeroCont'];
                    $_SESSION['dirVivienda']= $usuarioAux['dirVivienda'];
                    $_SESSION['tipoUsuario']= $usuarioAux['tipoUsuario'];
                    $_SESSION['auth']= true;

                    $usuarioAux=$_SESSION['usuario'];
                    $tipoUsuarioAux=$_SESSION['tipoUsuario'];
                    $query = "INSERT INTO historialinis (usuario, tipoUsuario, fechaIni) VALUES('$usuarioAux', '$tipoUsuarioAux', NOW())";
                    $resultado= mysqli_query($db, $query);
                    
                    if($_SESSION['tipoUsuario'] === 'admin'){
                        header('location: admin/');
                    }
                    if($_SESSION['tipoUsuario'] === 'cliente'){
                        header('location: \catalogo.php');
                    }
                   }else{
                        echo("<script>alert('El usuario o contraseña son incorrectos. Intente de nuevo') </script>");
                   }
                }else{
                    echo("<script>alert('El usuario o contraseña son incorrectos. Intente de nuevo') </script>");
                }
            }
        ?>
        <div class="logo">
            <a href="index.php"><img src="Imagenes/PrimateCuadrado.webp" alt="PrimateLogo" width="150" height="150" class="nav-brand"></a>
        </div>
        <section class="formularioLogin">
            <h4>Ingreso</h4>
            <form method="POST">
                <input class="controls" type="text" name="usuario" id="usuario" placeholder="Usuario" required="true" value="<?php echo("$usuario")?>">
                <input class="controls" type="password" name="contraseña" id="contraseña" placeholder="Contraseña"  required="true">
                <input class="boton" type="submit" value="Entrar">
            </form>
            <p>Aún no tengo una cuenta. <a class= "anima" href="Register.php">Deseo registrarme.</a></p>
        </section>
        <footer class="login">
            <a href='https://www.freepik.es/vectores/fondo'target="_blank">Vector de Fondo creado por freepik - www.freepik.es</a>
        </footer>
    </body>
</html>