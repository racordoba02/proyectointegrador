<?php include '../../../includes/templates/header.php'; ?>
<h1 class="titulo">Modificar</h1>
    <main class="modificar">
        <section class="formularioModificarProductos">
            <h4>Producto</h4>
            <?php
                require '../../../includes/config/CDB.php';
                $db = conectarDB();
                $idActu=filter_var($_GET['id'], FILTER_VALIDATE_INT);
                if(!$idActu){
                    header('Location: /admin');
                }
                //obtener los datos de las propiedades
                $consultaProductoActu="SELECT * FROM catalogo WHERE id = ${idActu}";
                $resultadoProductoActu= mysqli_query($db, $consultaProductoActu);
                $consultaProductoActu=mysqli_fetch_assoc($resultadoProductoActu);

                $nombreProducto = $consultaProductoActu['nombreProducto'];
                $precio = intval($consultaProductoActu['precio']);
                $tipoproducto= $consultaProductoActu['tipoProduc'];
                $tipochocolate= $consultaProductoActu['tipoChocolate'];
                $cantidad=intval($consultaProductoActu['cantidad']);
                $nombreImagen= $consultaProductoActu['imagen'];
                $descripcion = $consultaProductoActu['descripcion'];
        
                //Consulta para obtener los tipos de productos
                $consultaProductos = "SELECT * FROM tipoproducto";
                $resultadoProductos = mysqli_query($db, $consultaProductos);
                //Consulta para obtener los tipos de productos
                $consultaChocolate = "SELECT * FROM chocolate";
                $resultadoChocolate = mysqli_query($db, $consultaChocolate);

                if($_SERVER['REQUEST_METHOD']==='POST'){
                    $nombreProducto = mysqli_real_escape_string($db, $_POST['nombreProducto']);
                    $precio = mysqli_real_escape_string($db, $_POST['precio']);
                    $tipoproducto=  $_POST['tipoproducto'];
                    $tipochocolate= $_POST['tipochocolate'];
                    $cantidad= $_POST['cantidadEle'];
                    $descripcion = mysqli_real_escape_string($db, $_POST['descripcion']);
                    
                    $imagen=$_FILES['imagen'];

                    $carpetaImagenes = '../../../imagenes/imagenesProductos/';
                    if(!is_dir($carpetaImagenes)){
                        mkdir($carpetaImagenes);
                    }
                    if($imagen['name']){
                        //eliminar imagen previa
                        unlink($carpetaImagenes . $nombreImagen);
                        //Generar nombre unico
                        $nombreImagen=md5(uniqid(rand(),true)).".jpg";
                        //subir la imagen
                        move_uploaded_file($imagen['tmp_name'], $carpetaImagenes . $nombreImagen);
                    }
                    //Actualizar base de datos
                    $query="UPDATE catalogo SET nombreProducto = '${nombreProducto}', precio = ${precio}, tipoProduc = ${tipoproducto}, tipoChocolate = ${tipochocolate}, cantidad = ${cantidad}, imagen='${nombreImagen}', descripcion = '${descripcion}', fecCreado = NOW() WHERE id = ${idActu}";
                    $resultado = mysqli_query($db, $query);
                    if($resultado){
                        //echo"insertado Correctamente";
                        header('location:../actualizar.php'); //Redireccionando
                    }
                }
            ?>
            <form class="formulario" method="POST" enctype="multipart/form-data">
                <input class= "controls" type="text" name="nombreProducto" id="nombreProducto" placeholder="Nombre del producto" required="true" value="<?php echo("$nombreProducto")?>">
                <input class= "controls" type="text" name="precio" id="precio" placeholder="Precio del producto" required="true" value="<?php echo("$precio")?>">
                <select class= "controls" name="tipoproducto" id="tipoproducto" required="true">
                    <option value="">-Seleccione Tipo producto-</option>
                    <?php while($row=mysqli_fetch_assoc($resultadoProductos)): ?>
                        <option <?php echo ($tipoproducto == $row['id'])?'selected':'';?> value="<?php echo($row['id']);?>"><?php echo($row['tipoProductoChoc']);?></option>
                    <?php endwhile;?> 
                </select>
                <select class= "controls" name="tipochocolate" id="tipochocolate" required="true" selected="<?php echo("$tipoProduc")?>">
                    <option value="">-Seleccione Tipo chocolate-</option>
                    <?php while($row=mysqli_fetch_assoc($resultadoChocolate)): ?>
                        <option  <?php echo ($tipochocolate== $row['id'])?'selected':'';?> value="<?php echo($row['id']);?>"><?php echo($row['chocolate']);?></option>
                    <?php endwhile;?> 
                </select>
                <label for="cantidadEle">Cantidad de los elementos</label>
                <input class= "controls" type="number" name="cantidadEle" id="cantidadEle" placeholder="1" min="1" max="100" required="true"value="<?php echo("$cantidad")?>">
                <label for="imagen">Subir imagen (Tamaño maximo 2MB)</label>
                <input class= "controls" type="file" name="imagen" id="imagen" accept="image/*">
                <textarea class= "controls" name="descripcion" id="descripcion" required="true" cols="30" rows="10"><?php echo("$descripcion")?></textarea>
                <input class="boton" type="submit" value="Actualizar producto">
            </form>
        </section>
        <div class="seccionMod">
            <img class="imagenMod" src="/Imagenes/imagenesProductos/<?php echo($nombreImagen);?>" alt="Imagen de producto">
            <a class="boton" href="/admin/productos/actualizar.php">Volver</a> 
        </div>
    </main>
<?php include '../../../includes/templates/footer.php'; ?>