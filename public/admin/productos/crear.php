<?php include '../../includes/templates/header.php'; ?>
<h1 class="titulo">Crear</h1>
    <main class="Contenedor">
        <section class="formularioCrearProductos">
            <h4>Producto</h4>
            <?php
                $nombreProducto = "";
                $precio = "";
                $tipoproducto="";
                $tipochocolate="";
                $cantidad="";
                $descripcion = "";
                require '../../includes/config/CDB.php';
                $db = conectarDB();
                //Consulta para obtener los tipos de productos
                $consultaProductos = "SELECT * FROM tipoproducto";
                $resultadoProductos = mysqli_query($db, $consultaProductos);
                //Consulta para obtener los tipos de productos
                $consultaChocolate = "SELECT * FROM chocolate";
                $resultadoChocolate = mysqli_query($db, $consultaChocolate);

                if($_SERVER['REQUEST_METHOD']==='POST'){

                    $nombreProducto = mysqli_real_escape_string($db, $_POST['nombreProducto']);
                    $precio = mysqli_real_escape_string($db, $_POST['precio']);
                    $tipoproducto=  $_POST['tipoproducto'];
                    $tipochocolate= $_POST['tipochocolate'];
                    $cantidad= $_POST['cantidadEle'];
                    $descripcion = mysqli_real_escape_string($db, $_POST['descripcion']);

                    $imagen=$_FILES['imagen'];                   
                    //Insertar imagenes
                    //creando y validando alojamiento
                    $carpetaImagenes = '../../imagenes/imagenesProductos';
                    if(!is_dir($carpetaImagenes)){
                        mkdir($carpetaImagenes);
                    }
                    //Generar nombre unico
                    $nombreImagen=md5(uniqid(rand(),true)).".jpg";
                    //subir la imagen
                    move_uploaded_file($imagen['tmp_name'], $carpetaImagenes ."/". $nombreImagen);
                    //Inserar en base de datos
                    $query="INSERT INTO catalogo (nombreProducto, precio, tipoProduc, tipoChocolate, cantidad, imagen, descripcion, fecCreado) VALUES('$nombreProducto', $precio, $tipoproducto, $tipochocolate, $cantidad, '$nombreImagen','$descripcion', NOW())";
                    //echo($query);
                    $resultado = mysqli_query($db, $query);
                    if($resultado){
                        //echo"insertado Correctamente";
                        header('location:../admin'); //Redireccionando
                    }
                }
            ?>
            <form class="formulario" method="POST" action="/admin/productos/crear.php" enctype="multipart/form-data">
                <input class= "controls" type="text" name="nombreProducto" id="nombreProducto" placeholder="Nombre del producto" required="true" value="<?php echo("$nombreProducto")?>">
                <input class= "controls" type="text" name="precio" id="precio" placeholder="Precio del producto" required="true" value="<?php echo("$precio")?>">
                <select class= "controls" name="tipoproducto" id="tipoproducto" required="true">
                    <option value="">-Seleccione Tipo producto-</option>
                    <?php while($row=mysqli_fetch_assoc($resultadoProductos)): ?>
                        <option  <?php echo ($tipoproducto == $row['id'])?'selected':'';?> value="<?php echo($row['id']);?>"><?php echo($row['tipoProductoChoc']);?></option>
                    <?php endwhile;?> 
                </select>
                <select class= "controls" name="tipochocolate" id="tipochocolate" required="true">
                    <option value="">-Seleccione Tipo chocolate-</option>
                    <?php while($row=mysqli_fetch_assoc($resultadoChocolate)): ?>
                        <option  <?php echo ($tipochocolate== $row['id'])?'selected':'';?> value="<?php echo($row['id']);?>"><?php echo($row['chocolate']);?></option>
                    <?php endwhile;?> 
                </select>
                <label for="cantidadEle">Cantidad de los elementos</label>
                <input class= "controls" type="number" name="cantidadEle" id="cantidadEle" placeholder="1" min="1" max="100" required="true">
                <label for="imagen">Subir imagen (Tamaño maximo 2MB)</label>
                <input class= "controls" type="file" name="imagen" id="imagen" accept="image/*" required="true">
                <textarea class= "controls" name="descripcion" id="descripcion" required="true" cols="30" rows="10"><?php echo("$descripcion")?></textarea>
                <input class="boton" type="submit" value="Registrar">
            </form>
        </section>
        <a class="boton" href="/admin">Volver</a>
    </main>
<?php include '../../includes/templates/footer.php'; ?>