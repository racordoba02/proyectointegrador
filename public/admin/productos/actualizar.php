<?php include '../../includes/templates/header.php'; ?>
<h1 class="titulo">Actualizar productos</h1>
    <main class="actualizar">
        <?php
            require '../../includes/config/CDB.php';
            $db = conectarDB();
            //Consulta para obtener los tipos de productos
            $consultaCatalogo = "SELECT * FROM catalogo";
            $resultadoCatalogo = mysqli_query($db, $consultaCatalogo);

            if($_SERVER['REQUEST_METHOD'] === 'POST'){
                $id=$_POST['id'];
                //var_dump($id);
                $id = filter_var($id, FILTER_VALIDATE_INT);
                if($id){
                    //eliminar imagen
                    $query="SELECT imagen FROM catalogo WHERE id = ${id}";
                    //echo $query;
                    $resultadoDel = mysqli_query($db, $query);
                    $consultaDel = mysqli_fetch_assoc($resultadoDel);
                    $nombreImagen = $consultaDel['imagen'];
                    if($resultadoDel){
                        $carpetaImagenes = '../../imagenes/imagenesProductos/';
                        //eliminar imagen previa
                        unlink($carpetaImagenes . $nombreImagen);
                    }
                    //elimino registro
                    $query="DELETE FROM catalogo WHERE id= ${id}";
                    $resultadoDel = mysqli_query($db, $query);
                    if($resultadoDel){
                        header('location: actualizar.php');
                    }
                }
            }    
        ?>
        <div class="existentes">
            <table class= "productos">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre producto</th>
                        <th>Precio</th>
                        <th>Tipo producto</th>
                        <th>Tipo chocolate</th>
                        <th>Cantidad</th>
                        <th>Imagen</th>
                        <th>Fecha cre/mod</th>
                        <th>Acción</th>
                    </tr>
                </thead>
                <tbody>
                    <?php while($row=mysqli_fetch_assoc($resultadoCatalogo)): ?>
                    <tr>
                        <td><?php echo( intval($row['id']));?></td>
                        <td><?php echo($row['nombreProducto']);?></td>
                        <td>$<?php echo($row['precio']);?></td>
                        <td>
                            <?php 
                                $idf=$row['tipoProduc'];
                                $consultaAux="SELECT * FROM tipoproducto WHERE id = ${idf}";
                                $resultadoAux= mysqli_query($db, $consultaAux);
                                $consultaFAux=mysqli_fetch_assoc($resultadoAux);
                                echo($consultaFAux['tipoProductoChoc']);
                            ?>
                        </td>
                        <td>
                            <?php 
                                $idf=$row['tipoChocolate'];
                                $consultaAux="SELECT * FROM Chocolate WHERE id = ${idf}";
                                $resultadoAux= mysqli_query($db, $consultaAux);
                                $consultaFAux=mysqli_fetch_assoc($resultadoAux);
                                echo($consultaFAux['chocolate']);
                            ?>
                        </td>
                        <td><?php echo($row['cantidad']);?></td>
                        <td><img class="imagenTabla" src="/Imagenes/imagenesProductos/<?php echo($row['imagen']);?>" alt="Imagen de producto"></td>
                        <td><?php echo($row['fecCreado']);?></td>
                        <td>
                            <a class="botonAmarillo" href="/admin/productos/actualizar/modificar.php<?php echo("?id=".$row['id']);?>">Actualizar</a>
                            <form class = "w-100" method="POST">
                                <input type="hidden" name="id" value="<?php echo($row['id'])?>">
                                <input class="botonRojo" type= "submit" value ="Eliminar">
                            </form>
                        </td>
                    <?php endwhile;?>
                    </tr>
                </tbody>
            </table>
        </div>
        <a class="boton" href="/admin">Volver</a>
    </main>
<?php include '../../includes/templates/footer.php'; 
    mysqli_close($db);
?>